$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "pips_slider/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "pips_slider_rails"
  s.version     = PipsSliderRails::VERSION
  s.authors     = ["stanislav nosovskyi"]
  s.email       = ["mypostboxst@gmail.com"]
  s.homepage    = "https://bitbucket.org/stas_n/pips_slider_rails"
  s.summary     = "pips slider assets"
  s.description = "pips slider for rails app"
  s.license     = "MIT"


  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]



end
