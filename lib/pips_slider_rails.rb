module PipsSliderRails
  class Engine < ::Rails::Engine
    config.assets.paths << File.expand_path("../../assets/stylesheets", __FILE__)
    config.assets.paths << File.expand_path("../../assets/javascripts", __FILE__)
  end
end
